var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var assets = require('postcss-assets'),
autoprefixer = require('autoprefixer'),
mqpacker = require('css-mqpacker'),
cssnano = require('cssnano');
var browserSync = require('browser-sync').create();
var serverPath  = 'app';
var configPaths = {
    img: {
        src: 'src/assets/img/**/*.*',
        dest: serverPath + '/assets/img'
    },
    css: {
        src: 'src/assets/css/**/*.*',
        dest: serverPath + '/assets/css'
    },
    sass: {
        src:  'src/assets/sass/**/*.*',
        file:  'src/assets/sass/main.scss',
        dest: serverPath + '/assets/css'
    },
    fonts: {
        src:  'src/assets/fonts/**/*.*',
        dest: serverPath + '/assets/fonts'
    },
    pug: {
        src:  'src/pages/**/*.pug',
        dest: serverPath
    },
    js: {
        src:       'src/assets/scripts/**/*',
        dest:      serverPath + 'assets/js',
        bootstrap: {
            key:     '__bootstrap__',
            replace: '../../bower_components/bootstrap/js/dist'
        }
    },
    bower: 'bower_components'
};


// pug
gulp.task('views', function buildHTML() {
    return gulp.src(configPaths.pug.src)
        .pipe($.plumber({ errorHandler: (err)=> {
            $.notify.onError({
            title: "Error " + err.plugin,
            message:  err.toString()
            })(err);
        }}))
        .pipe($.newer(serverPath))
        .pipe($.pug({
            pretty: true,
            data: {
                STATIC_URL: 'assets/',
                GOOGLE_APIKEY: 'AIzaSyDSJTCObm-uFy0WUcLTpBQZ9zACGqv98hY'
                
              }
        }))
        .pipe($.plumber.stop())
        .pipe(gulp.dest(serverPath))
        .pipe(browserSync.stream())
  });
//   css
gulp.task('cssplugin', function(){
    return gulp.src(configPaths.css.src)
    .pipe($.plumber({ errorHandler: (err)=> {
        $.notify.onError({
        title: "Error " + err.plugin,
        message:  err.toString()
        })(err);
    }}))
    .pipe($.concat('plugins.css'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(configPaths.css.dest))
    .pipe(browserSync.stream())
})
//   sass
gulp.task('sass', ['images'], function() {
      var postCssOpts = [
      assets({ loadPaths: [configPaths.img.dest] }),
      autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
    //   mqpacker
      ];
    //   if (!devBuild) {
    //     postCssOpts.push(cssnano);
    //   }
      return gulp.src(configPaths.sass.file)
        .pipe($.plumber({ errorHandler: (err)=> {
            $.notify.onError({
            title: "Error " + err.plugin,
            message:  err.toString()
            })(err);
        }}))
        .pipe($.sass({
          outputStyle: 'nested',
          imagePath: configPaths.img.dest,
          precision: 3,
          errLogToConsole: true
        }))
        .pipe($.postcss(postCssOpts))
        .pipe($.plumber.stop())
        .pipe(gulp.dest(configPaths.sass.dest))
        .pipe(browserSync.stream())
    });

// images
  gulp.task('images', function() {
    return gulp.src(configPaths.img.src)
      .pipe($.newer(configPaths.img.dest))
      .pipe($.imagemin({ optimizationLevel: 5 }))
      .pipe(gulp.dest(configPaths.img.dest))
      .pipe(browserSync.stream())      
  });
//   fonts
  gulp.task('fonts', function() {
    return gulp.src(configPaths.fonts.src)
      .pipe($.newer(configPaths.fonts.dest))
      .pipe(gulp.dest(configPaths.fonts.dest))
      .pipe(browserSync.stream())      
  });
//   js
gulp.task('js', function() {
    return gulp.src(configPaths.js.src)
        .pipe($.plumber({ errorHandler: (err)=> {
            $.notify.onError({
            title: "Error " + err.plugin,
            message:  err.toString()
            })(err);
        }}))
        // .pipe($.deporder())
        // .pipe($.concat('main.js'))
        .pipe($.plumber.stop())
        .pipe(gulp.dest(serverPath + '/assets/js/'))
        .pipe(browserSync.stream())
});
  
  

// tasks

    // server
    gulp.task('serve', ['fonts' ,'images', 'js', 'sass', 'cssplugin', 'views'], function () {
        browserSync.init({
        server: {
            baseDir: "./app",
            directory: true,
            serveStaticOptions: {
                extensions: ["html"]
            }
        },
        port: 8080,
        reloadDelay: 2000,
        open: false
        });
        gulp.watch(configPaths.sass.src, ['sass']);
        gulp.watch(configPaths.css.src, ['cssplugin']);
        gulp.watch(configPaths.js.src, ['js']);
        gulp.watch(configPaths.pug.src, ['views']);
        gulp.watch("app/*.html").on('change', browserSync.reload);
    })

    gulp.task('default', ['serve']);
