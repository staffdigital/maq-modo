$(function(){

// header scroll
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 0) {
			$('.header__fixed').addClass('scrolling');
		}else{
			$('.header__fixed').removeClass('scrolling');
		};
	});

	// Ancla scroll - AGREGAR CLASE DEL ENLACE
	$('.miclase').click(function() {
	if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
			var targetOffset = $target.offset().top;
			$('html,body').animate({scrollTop: targetOffset}, 1000);
			return false;
			}
		}
	});


	$('.header__list').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('header__list');
	$('.header__logo').clone().prependTo('.menu-sidebar-cnt').removeClass('header__logo').addClass('responsive-logo');
	$('.header__right__info__button').clone().appendTo('.menu-sidebar-cnt');

	//events: menu burguer
	function cerrar_nav() {
		$('.menu-mobile-open').removeClass('active');
		$('.menu-mobile-close').removeClass('active');
		$('.menu-sidebar').removeClass('active');
		$('.header-menu-overlay').removeClass('active');
		$('body').removeClass('active');
	};

	function abrir_nav(){
		$('.menu-mobile-open').addClass('active');
		$('.menu-mobile-close').addClass('active');
		$('.menu-sidebar').addClass('active');
		$('.header-menu-overlay').addClass('active');
		$('body').addClass('active');
	}

	$('.menu-mobile-close , .header-menu-overlay').click(function(event) {
		event.preventDefault();
		cerrar_nav();
	});	

	$('.menu-mobile-open').click(function(event) {
		abrir_nav()
	});

	$('.menu-sidebar .h-link-ancla, .responsive-logo').click(function(event) {
		cerrar_nav();
	});

	//detectando tablet, celular o ipad
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	
	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	  // tasks to do if it is a Mobile Device
	  function readDeviceOrientation() {
		if (Math.abs(window.orientation) === 90) {
			// Landscape
			cerrar_nav();
		} else {
			// Portrait
			cerrar_nav();
		}
	  }
	  window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function() {
			var estadomenu = $('.menu-responsive').width();
			if(estadomenu != 0){
				cerrar_nav();
			}
		});
	}
});

// ANCLA HEADER
// $(document).ready(function($) {
// 	function location_url(){
// 		var w_url = window.location.pathname
// 		$('a[data-url="'+w_url+'"]').trigger('click');
// 	}
// 	location_url();
// 	});

// 	function trackingLink() {
// 		var href = window.location.href; var url = href.split('?z=');
// 		history.pushState(null, null, url[0]);
// 	}

// 	 $(function () {
// 	 	function wLinkerNav(url_obj,home=false){
// 			if(home){
// 				window.history.pushState(200, "Indutexa", '/');
// 			}else{
// 				window.history.pushState(200, url_obj.attr('data-title'), url_obj.attr('data-url'));
// 			}
// 		}

// 		var b1_block = $('#slider');
// 		var b2_block = $('#descripcion');
// 		var b3_block = $('#inversion');
// 		var b4_block = $('#beneficios');
// 		var b5_block = $('#proyectos');
// 		var b6_block = $('#respaldo');
// 		var b7_block = $('#contacto');

// 		var menu_a1 = $(".menu_a1");
// 		var menu_a2 = $(".menu_a2");
// 		var menu_a3 = $(".menu_a3");
// 		var menu_a4 = $(".menu_a4");
// 		var menu_a5 = $(".menu_a5");
// 		var menu_a6 = $(".menu_a6");

// 		b2_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a1.addClass('active');
// 				wLinkerNav(menu_a1);
// 			}else{
// 				menu_a1.removeClass('active');
// 				wLinkerNav(menu_a1,true);
// 			}
// 		}, {
// 			offset:'40%'
// 		});

// 		b3_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a2.addClass('active');
// 				menu_a1.removeClass('active');
// 				wLinkerNav(menu_a2);

// 			}else{
// 				menu_a1.addClass('active');
// 				menu_a2.removeClass('active');
// 				wLinkerNav(menu_a1);
// 			}
// 		}, {
// 			offset:'40%'
// 		});

// 		b4_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a3.addClass('active');
// 				menu_a2.removeClass('active');
// 				wLinkerNav(menu_a3);

// 			}else{
// 				menu_a2.addClass('active');
// 				menu_a3.removeClass('active');
// 				wLinkerNav(menu_a2);
// 			}
// 		}, {
// 			offset:'40%'
// 		});

// 		b5_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a4.addClass('active');
// 				menu_a3.removeClass('active');
// 				wLinkerNav(menu_a4);

// 			}else{
// 				menu_a3.addClass('active');
// 				menu_a4.removeClass('active');
// 				wLinkerNav(menu_a3);
// 			}
// 		}, {
// 			offset:'40%'
// 		});

// 		b6_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a5.addClass('active');
// 				menu_a4.removeClass('active');
// 				wLinkerNav(menu_a5);

// 			}else{
// 				menu_a4.addClass('active');
// 				menu_a5.removeClass('active');
// 				wLinkerNav(menu_a4);			}
// 		}, {
// 			offset:'40%'
// 		});

// 		b7_block.waypoint(function(direction) {
// 			if (direction === 'down') {
// 				menu_a6.addClass('active');
// 				menu_a5.removeClass('active');
// 				wLinkerNav(menu_a6);

// 			}else{
// 				menu_a5.addClass('active');
// 				menu_a6.removeClass('active');
// 				wLinkerNav(menu_a5);
// 			}
// 		}, {
// 			offset:'40%'
// 		});

// 	});